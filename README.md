# Genefunk Template World
A [Foundry VTT](https://foundryvtt.com/) world that provides a read to use setup for the genefunk system and the obsidiangenefunk module.

## Bug Reports
If you discover an issue with the sheet, please report it in the [issue tracker](https://bitbucket.org/finalfrog/genefunk-world/issues) and ensure you include the following information:

1. The steps you took to encounter the issue.
2. What you expected to happen.
3. What actually happened.
4. Whether you encountered the issue in the foundry client or in a web browser, or both (and which browser and OS, if applicable).

In addition to the above information, it can be useful if you are able to do the following, too:

1. Press F12 to bring up the console and copy and paste any error messages you find there into the bug report.
2. Zip up your `actors.db` and `items.db` files and attach it to the report. It can be found in your foundry data directory under `Data/worlds/genefunk/data/`.

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

A copy of this license is available in the `LICENSE` file, or can alternatively be found here: [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
